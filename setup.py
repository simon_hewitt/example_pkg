"""
See https://packaging.python.org/tutorials/packaging-projects/
"""
import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup (
    name="example-pkg-fatsimon",
    version="0.0.1",
    author="Simon Hewitt",
    author_email = "simon.d.hewitt@gmail.com",
    description = "A sample Python package",
    long_description = long_description,
    long_description_content_type = "text/markdown",
    url="https://bitbucket.org/simon_hewitt/example_pkg/src/master/",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
