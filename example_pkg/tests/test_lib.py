"""
Both this lib and the tests are pretty meaningless
"""

import datetime
import sh_lib

def test_hours_to_go():
    start_time = datetime.datetime(2019, 4, 1, 12, 0,0)
    end_time = datetime.datetime(2019,4,1,17,0,0)
    t = sh_lib.MyTime(start_time, end_time)
    h = t.hours_to_go()
    assert isinstance(h, float)
