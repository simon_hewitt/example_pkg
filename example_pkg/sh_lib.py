"""
Test classes for packaging
Both this lib and the tests are pretty meaningless
"""

import datetime

class MyTime:
    def __init__(self, start_time, end_time):
        assert isinstance(start_time, datetime.datetime), "start_time and end_time must be datetime values"
        assert isinstance(end_time, datetime.datetime), "start_time and end_time must be datetime values"
        self.start_time = start_time
        self.end_time = end_time

    def hours_to_go(self):
        now = datetime.datetime.now()
        delta = self.end_time - now
        return delta.seconds/3600.0

