# example-pkg

## A sample Python package

Based on the tutorial at `https://packaging.python.org/tutorials/packaging-projects/`

And saved in bitbucket at: `https://bitbucket.org/simon_hewitt/example_pkg/src/master/`

Tutorial by Simon Hewitt, at simon.d.hewitt@gmail.com

